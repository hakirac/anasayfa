import React, { Component } from 'react';
import logo from './PTLogo.svg';
import './App.css';
import Iframe from 'react-iframe'
import aktifLink from './aktifLink'

class App extends Component {
  constructor(prop) {
    super()
    this.state =  {
      url:"http://cbshozdasci/bidb",
      aktifLink:"analink1"
    }
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(event) {    
    aktifLink("anaLinkler", event, "secili") // secili linke css icin class "secili" class ini ekliyor
  }



  render() {

    return (
      <div className="App" >
        <header className="App-header" >
          <div className="logo-cevreleyici">
          <img src={logo} align="middle" className="ibbLogo" alt="İBB Logo" />
          <span> POTA (Proje Takip Sistemi) </span>
          </div>
          <ul id="anaLinkler" className = "header-linkler" >
            <li>              
              <a id="analink1" className="App-link secili" onClick = {this.handleClick} href={this.state.url+"/portfolyo"} target="iframe" >
                Proje Portfolyosu
              </a>
            </li>
            <li>
              <a id="analink2" className="App-link" onClick = {this.handleClick}  href={this.state.url+"/dashboard"} target="iframe">
                Dashboard
              </a>
            </li>
            <li>
              <a id="analink3" className="App-link" onClick = {this.handleClick}  href={this.state.url+"/paydas"} target="iframe">
                Paydaş Matrisi
              </a>
            </li>
            <li>
              <a id="analink4" className="App-link" onClick = {this.handleClick}  href={this.state.url+"/belgeler"} target="iframe">
                Belgeler
              </a>
            </li>
            <li>
              <a id="analink5" className="App-link" onClick = {this.handleClick} href={this.state.url+"/yukleme"} target="iframe" >
                Dosya Yukleme
              </a>
            </li>
          </ul>
        </header>
        
        <Iframe url={this.state.url+"/portfolyo"}
        width="100%"
        height="86%"
        id="myId"
        className="iframe"
        display="initial"
        position="absolute"
        name="iframe"
        allowFullScreen />
        <footer>
            <span>© Tüm hakkı İBB'ye aittir. <br /> Bilgi İşlem Daire Başkanlığı / Proje Yönetim Ofisi v1.0.0 </span>

        </footer>
      </div>
      
    );
  }
}

export default App;
